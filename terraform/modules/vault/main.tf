provider "docker" {
  host = "tcp://10.0.1.170:2376"
}

terraform {
  required_providers {
    docker = {
      source = "kreuzwerker/docker"
    }
  }
}

resource "docker_container" "consul" {
  image = "hashicorp/consul:latest"
  name  = "consul"
  ports {
    internal = 8500
    external = 8500
  }
  command = ["agent", "-dev", "-client", "0.0.0.0"]
}

resource "docker_image" "consul" {
  name = "hashicorp/consul:latest"
}

resource "docker_container" "vault" {
  image = "hashicorp/vault:latest"
  name  = "vault"
  ports {
    internal = 8200
    external = 8200
  }
  volumes {
    host_path      = "/opt/vault/config"
    container_path = "/config"
  }
  env = ["VAULT_ADDR=http://127.0.0.1:8200"]
  depends_on = [docker_container.consul]
}

resource "docker_image" "vault" {
  name = "hashicorp/vault:latest"
}
